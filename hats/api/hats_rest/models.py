from django.db import models

# Create your models here.
class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    picture = models.URLField(max_length=500, null=True)
    location = models.ForeignKey(
        "LocationVO",
        related_name="hats",
        on_delete=models.CASCADE
    )
    
    def __str__(self):
        return self.style
    
class LocationVO(models.Model):
    shelf_number = models.PositiveSmallIntegerField(null=True)
    closet_name = models.CharField(null=True, max_length=100)
    section_number = models.PositiveSmallIntegerField(null=True)
    import_href = models.CharField(max_length=100, unique=True)
    
    