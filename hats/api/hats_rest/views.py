from django.shortcuts import render
from common.json import ModelEncoder
from hats_rest.models import Hat, LocationVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

# Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
        "shelf_number",
        "section_number",
    ]
    
class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "color",
        "style_name",
        "picture",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style_name",
        "color",
        "picture",
        "fabric",
    ]
    def get_extra_data(self, o):
        return {"location": o.location.closet_name}
    
@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id = None):
    if request.method == "GET":
        if location_vo_id is not None:
            hat = Hat.objects.filter(location_vo_id=location_vo_id)
        else:
            hat = Hat.objects.all()
            return JsonResponse(
                {"hats": hat},
                encoder=HatListEncoder,
            )
    else:
        content = json.loads(request.body)
        
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hat(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Hat.objects.filter(id=id).update(**content)
        
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )