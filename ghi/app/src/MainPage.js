function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <div className="purple-box" style={{ position: "relative", display: "inline-block", padding: "20px", backgroundColor: "skyblue", borderRadius: "10px", boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.2)", transition: "transform 0.3s ease-in-out" }}>
        <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-2">
            Need to keep track of your shoes and hats?
          </p>
          <p className="lead mb-2">
            We have the solution for you!
          </p>
          <img src="https://us.123rf.com/450wm/aleutie/aleutie2104/aleutie210400037/168760756-woman-pushing-the-doors-of-the-closet-shut-keeping-her-demons-inside-eps-8-vector-illustration.jpg?ver=6" style={{ width: "160px", height: "240px" }} />
          <div>
            <a href="http://localhost:3000/shoes/list" className="btn btn-primary mt-3 me-2">Shoes</a>
            <a href="http://localhost:3000/hats/list" className="btn btn-primary mt-3">Hats</a>
          </div>
        </div>
      </div>
      <div className="blue-box" style={{ position: "relative", display: "inline-block", padding: "20px", backgroundColor: "lightblue", borderRadius: "10px", boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.2)", transition: "transform 0.3s ease-in-out", marginTop: "20px" }}>
        <p className="lead mb-2">
          Tell us what clothes in your closet you'd like to add to this app!
        </p>
        <textarea className="form-control mt-3" rows="3"></textarea>
      </div>
    </div>


  );
}

export default MainPage;
