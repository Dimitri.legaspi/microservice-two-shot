import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById("root"));

async function loadData() {
  const responseShoes = await fetch("http://localhost:8080/api/shoes/");
  const responseHats = await fetch("http://localhost:8090/api/hats/");
  if (responseShoes.ok && responseHats.ok) {
    const dataShoes = await responseShoes.json();
    const dataHats = await responseHats.json();
    root.render(
      <React.StrictMode>
        <App shoes={dataShoes.shoes} hats={dataHats.hats} />
      </React.StrictMode >
    )
  }
}

loadData();



// async function loadShoes() {
//   const response = await fetch("http://localhost:8080/api/shoes/");
//   if (response.ok) {
//     const data = await response.json();
//     console.log(data);
//     root.render(
//       <React.StrictMode>
//         <App shoes={data.shoes} />
//       </React.StrictMode >
//     )
//   }
// }
// loadShoes();

// async function loadHats() {
//   const response = await fetch("http://localhost:8090/api/hats/");
//   if (response.ok) {
//     const data = await response.json();
//     console.log(data);
//     root.render(
//       <React.StrictMode>
//         <App hats={data.hats} />
//       </React.StrictMode >
//     )
//   }
// }
// loadHats();
