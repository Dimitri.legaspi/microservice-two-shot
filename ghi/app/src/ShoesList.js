import React from 'react';
import "./index.css"

function ShoesList(props) {
    const removeShoe = (id) => {
        if (window.confirm("Delete?")) {
            fetch("http://localhost:8080/api/shoes/" + id, { method: "DELETE" }).then(() => {
                window.location.reload();
            }).catch((err) => {
                console.error(err);
            });
        }
    }
    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Picture</th>
                        <th>Manufacturer</th>
                        <th>Model Name</th>
                        <th>Color</th>
                        <th>Bin</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {props.shoes?.map(shoe => {
                        return (
                            <tr key={shoe.href}>
                                <td><img src={shoe.picture_url} style={{ width: "40px", height: "60px" }} /> </td>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.bin.closet_name}</td>
                                <td><button onClick={() => { removeShoe(shoe.id) }}>Delete</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table >
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            </div>
        </div >
    );
}

export default ShoesList;
