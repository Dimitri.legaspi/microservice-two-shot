import React from "react";
import './index.css';


function HatsList(props) {
    const removeHat = (id) => {
        if (window.confirm("Do you want to delete this item?")) {
            fetch("http://localhost:8090/api/hats/" + id, { method: "DELETE" }).then(() => {
                window.location.reload();
            }).catch((err) => {
                console.log(err.message);
            })
        }
    }
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Hat Style</th>
                    <th>Hat Color</th>
                    <th>Hat Fabric</th>
                    <th>Hat Picture</th>
                    <th>Locations</th>
                </tr>
            </thead>
            <tbody>
                {props.hats?.map(hat => {
                    console.log("rendering hat")
                    return (
                        <tr key={hat.id}>
                            <td>
                                {hat.style_name}
                            </td>
                            <td>
                                {hat.color}
                            </td>
                            <td>
                                {hat.fabric}
                            </td>
                            <td>
                                <img src={hat.picture} alt="Hat" />
                            </td>
                            <td>
                                {hat.location}
                            </td>
                            <td>
                                <button className="btn btn-danger" onClick={() => { removeHat(hat.id) }}>
                                    Delete
                                </button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default HatsList;
