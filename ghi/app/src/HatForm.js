import React, { useEffect, useState} from "react";

function HatForm() {
    const [locations, setLocations] = useState([]);
    const [color, setColor] = useState('');
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [picture, setPicture] = useState('');
    const [location, setLocation] = useState('');

    const colorChanged = (event) => {
      setColor(event.target.value);
    }
    const fabricChanged = (event) => {
      setFabric(event.target.value);
    }
    const styleChanged = (event) => {
      setStyle(event.target.value);
    }
    const pictureChanged = (event) => {
      setPicture(event.target.value);
    }
    const locationChanged = (event) => {
      setLocation(event.target.value);
    }

    const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {};
      data.color = color;
      data.fabric = fabric;
      data.style_name = style;
      data.picture = picture;
      data.location = location;
      console.log(data);

      const url = "http://localhost:8090/api/hats/";
      const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(url, fetchConfig);
      if(response.ok){
        const newHat = await response.json();
        console.log(newHat);

        setColor('');
        setFabric('');
        setStyle('');
        setPicture('');
        setLocation('');
      }
    }
    
    const fetchData = async () => {
        const response = await fetch("http://localhost:8100/api/locations/");
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a New Hat</h1>
                <form id="create-conference-form" onSubmit={handleSubmit}>
                  <div className="form-floating mb-3">
                    <input placeholder="Color" required type="text" name="color" id="name" className="form-control" onChange={colorChanged} value={color}/>
                    <label htmlFor="color">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" onChange={fabricChanged} value={fabric}/>
                    <label htmlFor="fabric">Fabric</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="Style" required type="text" name="syle_name" id="styel_name" className="form-control" onChange={styleChanged} value={style}/>
                    <label htmlFor="style_name">Style</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="Picture URL" required type="url" name="url" id="url" className="form-control" onChange={pictureChanged} value={picture}/>
                    <label htmlFor="url">Picture URL </label>
                  </div>
                  <div className="mb-3">
                    <select required  name="location" id="location" className="form-select" onChange={locationChanged} value={location}>
                      <option value="">Choose a location</option>
                      {locations.map(location => {
                        return (
                            <option value={location.href} key={location.href}>
                                {location.closet_name}
                            </option>
                        );
                    })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
    );
}

export default HatForm;
