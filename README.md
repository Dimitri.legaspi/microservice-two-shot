# Wardrobify

Team:

* Dimitri Legaspi- Hats Microservice
* Johnny Belknap - Shoes Microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

In the hats microservice, I included fields for the style of the hat, the color, and the fabric of the hat, a picture of the hat, and the location to which the hat belongs. The Hat Model is connected to the Wardrobe API via a Location Value Object.